# /intranet-workflow/www/inbox-action.tcl

ad_page_contract {
    Assign a specific task to a user
} {
    task_id:integer
    { assignee_id "" }
    return_url
}


set current_user_id [auth::require_login]
if { $assignee_id eq "" } { set assignee_id $current_user_id }

# Check if the current user is assigned to the task and try to self-assign if not.
set current_user_assigned_p [db_string task_assigned_users "
    		select	count(*)
		from	wf_user_tasks ut
		where	ut.task_id = :task_id and
			ut.user_id = :current_user_id
	    "]

set reassign_allowed_p 0
set manager_p 0
if {!$current_user_assigned_p} {
    # Check if they are a manager of somebody who is assigned to the task
    set manager_p [db_string manager "select 1 from im_employees, wf_task_assignments where supervisor_id = :current_user_id and employee_id = party_id and task_id = :task_id" -default 0]
    if {$manager_p} {
	set reassign_allowed_p 1
    }
} else {
    # Check that the assignee is somebody for whom they are a supervisor
    set supervisor_p [db_string supervisor "select 1 from im_employees where employee_id = :assignee_id and supervisor_id = :current_user_id" -default 0]
    if {$supervisor_p} {
	set reassign_allowed_p 1
    }
}


if {$reassign_allowed_p} {
    wf_case_add_task_assignment -task_id $task_id -party_id $assignee_id

    set case_id [db_string case_id "select case_id from wf_tasks where task_id = :task_id" -default ""]
    if {$case_id ne "" } {
	if {$manager_p} {
	    wf_new_journal -case_id $case_id -action assign_to_manager -action_pretty "Assign to manager" -message "Assigning to [im_name_from_id $assignee_id] in his role as a manager"
	} else {
    	    wf_new_journal -case_id $case_id -action assign_from_supervisor -action_pretty "Assign by supervisor" -message "Assigning to [im_name_from_id $assignee_id] by the supervisor [im_name_from_id $current_user_id]"
	}
    }
}

ad_returnredirect $return_url




